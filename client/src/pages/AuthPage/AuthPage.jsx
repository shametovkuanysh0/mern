import React, { useState } from 'react'
import './AuthPage.scss'
import { BrowserRouter, Switch, Route, Link} from 'react-router-dom'
import axios from 'axios'

const AuthPage = () => {
    const [form, setForm] = useState({
        email: '',
        password: ''
    })

    const changeHandler = (event) => {
        setForm({...form, [event.target.name]:event.target.value})
        console.log(form);
    }

    const registerEvent = async () => {
        try {
            await axios.post('/api/auth/registration',{...form}).then({
                
            })
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <BrowserRouter>
            <Switch>
                <React.Fragment>
                    <div className="container">
                        <div className="auth-page">
                            <Route path="/login">
                                <h3>Авторизация</h3>
                                <form className="form form-login">
                                    <div className="row">
                                        <div className="input-field col s12">
                                            <input type="email" 
                                                    name="email"
                                                    className="validate"
                                                    onChange={changeHandler}
                                            />
                                            <label htmlFor="email">Email</label>
                                        </div>
                                        <div className="input-field col s12">
                                            <input type="password" 
                                                    name="password"
                                                    className="validate"
                                                    onChange={changeHandler}
                                            />
                                            <label htmlFor="password">Пароль</label>
                                        </div>
                                    </div>
                                    <div className = "row">
                                        <button className="waves-effect waves-light btn blue">
                                            Войти
                                        </button>
                                        <Link to="/registration" className="btn-outline btn-reg">Нет акаунта?</Link>
                                    </div>
                                </form>
                            </Route>

                            <Route path="/registration">
                                <h3>Регистрация</h3>
                                <form className="form form-login">
                                    <div className="row">
                                        <div className="input-field col s12">
                                            <input type="email" 
                                                    name="email"
                                                    className="validate"
                                                    onChange={changeHandler}
                                            />
                                            <label htmlFor="email">Email</label>
                                        </div>
                                        <div className="input-field col s12">
                                            <input type="password" 
                                                    name="password"
                                                    className="validate"
                                                    onChange={changeHandler}
                                            />
                                            <label htmlFor="password">Пароль</label>
                                        </div>
                                    </div>
                                    <div className = "row">
                                        <button className="waves-effect waves-light btn blue">
                                            Регистрация
                                        </button>
                                        <Link to="/login" className="btn-outline btn-reg">Уже есть аккаунт?</Link>
                                    </div>
                                </form>
                            </Route>
                        </div>
                    </div>
                </React.Fragment>
            </Switch>
        </BrowserRouter>
    )
}

export default AuthPage