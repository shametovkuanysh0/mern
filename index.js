const express = require('express')
const mongoose = require('mongoose')

const app = express()
const PORT = 5000

app.use('/api/auth', require('./routes/auth.route'))

async function start() {
    try {   
        await mongoose.connect('mongodb+srv://Lol:lol@cluster0.b6al2.mongodb.net/todo?retryWrites=true&w=majority', {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
      
        app.listen(PORT, () => {
            console.log('Server is started on port 5000');
        })

    } catch(err) {console.error(err)}
}

start()